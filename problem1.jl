### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 7bc88700-b000-11eb-0302-89ce5dfe6e20
using Pkg

# ╔═╡ 02e055c7-d1aa-4a33-8d39-d93816e4f959
using DataStructures

# ╔═╡ ab9cd3d7-5490-4e5d-a5f4-0be85c5bcd2b
Pkg.add("DataStructures")

# ╔═╡ 83fb292e-23f7-4c4e-b685-75c457e7a12b
struct State
	name::String
	position::Int64
	number_of_dirt::Vector{Int}
end

# ╔═╡ 1534e8ee-ed5f-4ead-8e0f-090ccebe2269
struct Action
	name::String
	cost::Int64
end

# ╔═╡ 07ddc62a-b941-49e8-91ac-2d39dcb8c436
A1 = Action("mw", 3)

# ╔═╡ 48f37c66-1908-4e2a-b77b-0686556ace56
A2 = Action("me", 3)

# ╔═╡ 9fad5628-a7d1-417c-a078-a00ee3a19186
A3 = Action("re", 1)

# ╔═╡ 20d8d5e1-4f8b-4ea8-942e-dcdd953c73f8
A4 = Action("co", 5)

# ╔═╡ 70740ca2-f361-41da-af94-12f2ce745652
S1 = State("State one", 1, [1, 3, 2, 1])

# ╔═╡ 47228049-648a-4895-a7f5-5893bbeae3ed
S2 = State("State two", 2, [1, 3, 2, 1])

# ╔═╡ 48088da2-ad5b-490b-99df-c318ee427913


# ╔═╡ 7ee35196-2578-433f-a52a-78ef7bddd2ab
S3 = State("State three", 3, [1, 3, 2, 1])

# ╔═╡ 3bd1b3bc-b011-4a08-8208-76834ea6d787
S4 = State("State four", 4, [1, 3, 2, 1])

# ╔═╡ 36e65be3-c104-4790-ab57-57740a6f6863
S5 = State("State five", 1, [0, 3, 2, 1])

# ╔═╡ 1cd8cbc5-9885-4f0e-aab1-fee4e398e018
S6 = State("State six", 2, [0, 3, 2, 1])

# ╔═╡ cc192893-54d4-4a85-95ea-217006df0b55
S7 = State("State seven", 3, [0, 3, 2, 1])

# ╔═╡ 212021b5-3480-49fe-8682-6852e00c0d13
S8 = State("State eight", 4, [0, 3, 2, 1])

# ╔═╡ 9598248c-3d3e-46d4-9a0b-3a84a0823fc0
S9 = State("State nine", 4, [0, 3, 2, 0])

# ╔═╡ f3e48150-b0da-11eb-3488-2308a2282783
S10 = State("State ten", 1, [0, 2, 2, 1])

# ╔═╡ 86210750-b0db-11eb-0dca-93551e1c97e5
S11 = State("State eleven", 2, [0, 2, 2, 1])

# ╔═╡ a428680e-b0db-11eb-18fa-536ee1b9ff0a
S12 = State("State twelve", 3, [0, 2, 2, 1])

# ╔═╡ be5ca0c0-b0db-11eb-3d55-3959a80d1925
S13 = State("State thirteen", 4, [0, 2, 2, 1])

# ╔═╡ ce544e10-b0db-11eb-08a0-838978066845
S14 = State("State fourteen", 1, [0, 1, 2, 1])

# ╔═╡ 017a0640-b0dc-11eb-1ffd-c158b53475d0
S15 = State("State fifteen", 2, [0, 1, 2, 1])

# ╔═╡ 07e1bbe0-b0dc-11eb-3326-8b6f0bde15d6
S16 = State("State sixteen", 3, [0, 1, 2, 1])

# ╔═╡ 19e90f4e-b0dc-11eb-2ec5-ede3edbf46a6
S17 = State("State seventeen", 4, [0, 1, 2, 1])

# ╔═╡ 2c848c70-b0dc-11eb-0301-99267f65b1ee
S18 = State("State eighteen", 1, [0, 0, 2, 1])

# ╔═╡ 50f55080-b0dc-11eb-3489-052ca4e4c6b1
S19 = State("State nineteen", 2, [0, 0, 2, 1])

# ╔═╡ 603dab00-b0dc-11eb-33d8-91eea892bf75
S20 = State("State twenty", 3, [0, 0, 2, 1])

# ╔═╡ 72936c90-b0dc-11eb-0b7a-5794ad37087c
S21 = State("State twentyone", 4, [0, 0, 2, 1])

# ╔═╡ 82d2d140-b0dc-11eb-35f2-4b33e101c9fe
S22 = State("State twentytwo", 1, [0, 0, 1, 1])

# ╔═╡ 956d15e0-b0dc-11eb-26d2-cd5a7fb80401
S23 = State("State twentythree", 2, [0, 0, 1, 1])

# ╔═╡ 9fa02b60-b0dc-11eb-2bcc-cb5a9678a3ad
S24 = State("State twentyfour", 3, [0, 0, 1, 1])

# ╔═╡ b25041f0-b0dc-11eb-3320-85e06ba28aa1
S25 = State("State twentyfive", 4, [0, 0, 1, 1])

# ╔═╡ bc174f30-b0dc-11eb-1141-f5e3b0f12dcb
S26 = State("State twentysix", 1, [0, 0, 0, 1])

# ╔═╡ dee33ce0-b0dc-11eb-3c88-e9d8b7f1d36b
S27 = State("State twentyseven", 2, [0, 0, 0, 1])

# ╔═╡ eac47790-b0dc-11eb-01af-b5579ab866dc
S28 = State("State twentyeight", 3, [0, 0, 0, 1])

# ╔═╡ 66245780-b0e1-11eb-374a-07278d312427
S29 = State("State twentynine", 4, [0, 0, 0, 1])

# ╔═╡ f559d150-b0dc-11eb-24be-cbfc215a7ba2
S30 = State("State thirty", 4, [0, 0, 0, 0])

# ╔═╡ ff246100-b0dc-11eb-0a42-53a061c9f18d
S31 = State("State thirtyone", 1, [0, 3, 2, 0])

# ╔═╡ a18326c0-b0dd-11eb-2da1-6fa5e4b5c329
S32 = State("State thirtytwo", 2, [0, 3, 2, 0])

# ╔═╡ aa5c5190-b0dd-11eb-0c2f-4b7e4fed6582
S33 = State("State thirtythree", 3, [0, 3, 2, 0])

# ╔═╡ b581dd60-b0dd-11eb-0a5a-190ae5deb928
S34 = State("State thirtyfour", 1, [0, 3, 1, 0])

# ╔═╡ d84a6fb0-b0dd-11eb-0b76-518954ae19ba
S35 = State("State thirtyfive", 2, [0, 3, 1, 0])

# ╔═╡ e6f8d422-b0dd-11eb-0eb8-d5d419d1eeb8
S36 = State("State thirtysix", 3, [0, 3, 1, 0])

# ╔═╡ f0f437d0-b0dd-11eb-2d8c-2deeb73cc6cd
S37 = State("State thirtyseven", 4, [0, 3, 1, 0])

# ╔═╡ fa72f170-b0dd-11eb-2cfb-a99dd0b6a632
S38 = State("State thirtyeight", 1, [0, 3, 0, 0])

# ╔═╡ 0b58f660-b0de-11eb-165d-d9887ad4d8c7
S39 = State("State thirtynine", 2, [0, 3, 0, 0])

# ╔═╡ 19a9d17e-b0de-11eb-0020-6b221513fb21
S40 = State("State fourty", 3, [0, 3, 0, 0])

# ╔═╡ 2c71dce2-b0de-11eb-3754-9d82316213c6
S41 = State("State fourtyone", 4, [0, 3, 0, 0])

# ╔═╡ 341ac510-b0de-11eb-3d5c-f9df00ec2842
S42 = State("State fourtytwo", 1, [0, 2, 0, 0])

# ╔═╡ 48903250-b0de-11eb-3719-f74a21f20b23
S43 = State("State fourtythree", 2, [0, 2, 0, 0])

# ╔═╡ 55c76010-b0de-11eb-3f8e-970ec370007d
S44 = State("State fourtyfour", 3, [0, 2, 0, 0])

# ╔═╡ 6a019912-b0de-11eb-1e5b-3b9525c40138
S45 = State("State fourtyfive", 4, [0, 2, 0, 0])

# ╔═╡ 758c9d70-b0de-11eb-363a-c730c48a160b
S46 = State("State fourtysix", 1, [0, 1, 0, 0])

# ╔═╡ 850cf7e0-b0de-11eb-2ffd-f51b6b871855
S47 = State("State fourtyseven", 2, [0, 1, 0, 0])

# ╔═╡ 92593440-b0de-11eb-2218-e706ba92d4ac
S48 = State("State fourtyeight", 3, [0, 1, 0, 0])

# ╔═╡ 9c28a5f0-b0de-11eb-1c38-c147f95d1f80
S49 = State("State fourtysix", 4, [0, 1, 0, 0])

# ╔═╡ a29145f0-b0de-11eb-3b5e-b525b7f695d4
S50 = State("State fifty", 2, [0, 0, 0, 0])

# ╔═╡ 98b223ef-89a6-4425-bbec-805867c21fe1
Transition_Mod = Dict()

# ╔═╡ b7cd3ea9-dec5-4fdb-aa9a-868a8a48ef56
push!(Transition_Mod, S1 => [(A2, S2), (A1, S1), (A3, S1), (A4, S5)])

# ╔═╡ 2090e0a1-9c47-4ebe-8282-6f61c10b0015
push!(Transition_Mod, S2 => [(A2, S3), (A1, S1), (A3, S2)])

# ╔═╡ 46841b1b-f8f2-4d2b-8e1c-2986a8c40086
push!(Transition_Mod, S3 => [(A2, S4), (A1, S2), (A3, S3)])

# ╔═╡ 858424cb-ebe3-4363-9c1a-0370689db1a1
push!(Transition_Mod, S4 => [(A2, S4), (A1, S3), (A3, S4)])

# ╔═╡ 70ff28a4-34af-4fc2-ae07-dadbfe35bf5c
push!(Transition_Mod, S5 => [( A1, S5), (A2, S6), (A3, S5)])

# ╔═╡ e8a05ca5-857e-458f-a248-92a173321ed5
push!(Transition_Mod, S6 => [( A1, S5), (A2, S7), (A3, S6), (A4, S11)])

# ╔═╡ 1b2a8067-6e06-4402-88ac-a1215fd85f0a
push!(Transition_Mod, S7 => [( A1, S6), (A2, S8), (A3, S7)])

# ╔═╡ 71a079e0-b4e7-4450-ab11-30a9be19da7e
push!(Transition_Mod, S8 => [( A1, S7), (A2, S8), (A3, S8), (A4, S9)])

# ╔═╡ e88c01d0-b0de-11eb-06cd-a7b5ed0ffc55
push!(Transition_Mod, S9 => [( A1, S33), (A2, S9), (A3, S9)])

# ╔═╡ 18800210-b0df-11eb-3ccf-1123101fb30e
push!(Transition_Mod, S10 => [( A1, S10), (A2, S11), (A3, S10)])

# ╔═╡ 35d59f02-b0df-11eb-1258-2303deae91cb
push!(Transition_Mod, S11 => [( A1, S10), (A2, S12), (A3, S11), (A4, S15)])

# ╔═╡ 53d844d0-b0df-11eb-05b4-61f06de32a9e
push!(Transition_Mod, S12 => [( A1, S11), (A2, S13), (A3, S12)])

# ╔═╡ 74c11aa0-b0df-11eb-2daf-53d063f95e3a
push!(Transition_Mod, S13 => [( A1, S12), (A2, S13), (A3, S13)])

# ╔═╡ 87ee5070-b0df-11eb-3fef-7ba36a007252
push!(Transition_Mod, S14 => [( A1, S14), (A2, S15), (A3, S14)])

# ╔═╡ aeeb1690-b0df-11eb-39bf-6952d8fabcf7
push!(Transition_Mod, S15 => [( A1, S14), (A2, S15), (A3, S15), (A4, S19)])

# ╔═╡ dddd1020-b0df-11eb-0161-ed2a43dfbc49
push!(Transition_Mod, S16 => [( A1, S15), (A2, S16), (A3, S16)])

# ╔═╡ f53ec7e0-b0df-11eb-1c47-a943d073bb20
push!(Transition_Mod, S17 => [( A1, S16), (A2, S17), (A3, S17)])

# ╔═╡ 029c906e-b0e0-11eb-038e-0ff1f2b14530
push!(Transition_Mod, S18 => [( A1, S18), (A2, S19), (A3, S18)])

# ╔═╡ 15798360-b0e0-11eb-104a-8155eefdd2fa
push!(Transition_Mod, S19 => [( A1, S18), (A2, S20), (A3, S19)])

# ╔═╡ 2a52bb7e-b0e0-11eb-229c-43b958c8044e
push!(Transition_Mod, S20 => [( A1, S19), (A2, S21), (A3, S20), (A4, S24)])

# ╔═╡ 55811d60-b0e0-11eb-3fec-8ba83e9f7d5a
push!(Transition_Mod, S21 => [( A1, S20), (A2, S21), (A3, S21)])

# ╔═╡ 714e4592-b0e0-11eb-2430-79cec017dbb4
push!(Transition_Mod, S22 => [( A1, S22), (A2, S23), (A3, S22)])

# ╔═╡ 7f830d30-b0e0-11eb-12ee-7b5e70cd4ff8
push!(Transition_Mod, S23 => [( A1, S22), (A2, S24), (A3, S23)])

# ╔═╡ ad938060-b0e0-11eb-2f69-e7c3632a2ee1
push!(Transition_Mod, S24 => [( A1, S23), (A2, S25), (A3, S24), (A4, S28)])

# ╔═╡ c7b47f32-b0e0-11eb-0250-87fca7ebf2e8
push!(Transition_Mod, S25 => [( A1, S24), (A2, S25), (A3, S25)])

# ╔═╡ e87f6cc0-b0e0-11eb-38fc-690cdaf3d4df
push!(Transition_Mod, S26 => [( A1, S26), (A2, S27), (A3, S26)])

# ╔═╡ fbb3f590-b0e0-11eb-265e-99cc5f0e141d
push!(Transition_Mod, S27 => [( A1, S26), (A2, S28), (A3, S27)])

# ╔═╡ 07ac88ce-b0e1-11eb-2023-6f13406f68ef
push!(Transition_Mod, S28 => [ (A2, S29), (A3, S28)])

# ╔═╡ 3dc9af10-b0e1-11eb-33d7-59125ac786aa
push!(Transition_Mod, S29 => [( A1, S28), (A2, S29), (A3, S29), (A4, S30)])

# ╔═╡ 208d1750-b1b0-11eb-2cfe-bd389943e680
push!(Transition_Mod, S30 => [ (A3, S30)])

# ╔═╡ 9dac5860-b0e1-11eb-20cf-9799286abf46
push!(Transition_Mod, S31 => [( A1, S31), (A2, S32), (A3, S31)])

# ╔═╡ 79fa0250-b0e1-11eb-3e1d-396296587878
push!(Transition_Mod, S32 => [( A1, S31), (A2, S33), (A3, S32)])

# ╔═╡ 2dc4ecf0-b0e2-11eb-1983-43f43f5fe66d
push!(Transition_Mod, S33 => [( A1, S32), (A2, S9), (A3, S33), (A4, S36)])

# ╔═╡ 4e318de0-b0e2-11eb-28cc-f13ba0b3d20d
push!(Transition_Mod, S34 => [( A1, S34), (A2, S35), (A3, S34)])

# ╔═╡ 6b47fef0-b0e2-11eb-1ca0-e5d79380295a
push!(Transition_Mod, S35 => [( A1, S34), (A2, S36), (A3, S35)])

# ╔═╡ 7d899c40-b0e2-11eb-1ff4-7f3557f8f0bd
push!(Transition_Mod, S36 => [( A1, S35), (A2, S37), (A3, S36), (A4, S40)])

# ╔═╡ 9f99b400-b0e2-11eb-2937-ad49958987fa
push!(Transition_Mod, S37 => [( A1, S36), (A2, S37), (A3, S37)])

# ╔═╡ af1f3e92-b0e2-11eb-1222-45f3e530e39f
push!(Transition_Mod, S38 => [( A1, S38), (A2, S39), (A3, S38)])

# ╔═╡ c6002d90-b0e2-11eb-3f21-db614dc4427d
push!(Transition_Mod, S39 => [( A1, S38), (A2, S40), (A3, S39), (A4, S43)])

# ╔═╡ e3d66c30-b0e2-11eb-168f-a9773d38a88f
push!(Transition_Mod, S40 => [( A1, S39), (A2, S41), (A3, S40)])

# ╔═╡ f9239bd0-b0e2-11eb-0dbc-b970be493072
push!(Transition_Mod, S41 => [( A1, S40), (A2, S41), (A3, S41)])

# ╔═╡ 099226d0-b0e3-11eb-0bc0-d3f651f8a29c
push!(Transition_Mod, S42 => [( A1, S42), (A2, S43), (A3, S42)])

# ╔═╡ 1c5d6680-b0e3-11eb-3dff-1553f970a978
push!(Transition_Mod, S43 => [( A1, S42), (A2, S44), (A3, S43), (A4, S47)])

# ╔═╡ 593f2f70-b0e3-11eb-25d0-f112b757bcc2
push!(Transition_Mod, S44 => [( A1, S43), (A2, S45), (A3, S44)])

# ╔═╡ 6edaa620-b0e3-11eb-2d37-f564ee7c8758
push!(Transition_Mod, S45 => [( A1, S44), (A2, S45), (A3, S45)])

# ╔═╡ 7c213d30-b0e3-11eb-1e1d-eb7540938da6
push!(Transition_Mod, S46 => [( A1, S46), (A2, S47), (A3, S46)])

# ╔═╡ 90564610-b0e3-11eb-3603-a1c12f247a54
push!(Transition_Mod, S47 => [( A1, S46), (A2, S48), (A3, S47), (A4, S50)])

# ╔═╡ a52c49e0-b0e3-11eb-078d-0123d0cbbeea
push!(Transition_Mod, S48 => [( A1, S47), (A2, S49), (A3, S48)])

# ╔═╡ b9543360-b0e3-11eb-0bb3-3b261d9d6294
push!(Transition_Mod, S49 => [( A1, S48), (A2, S49), (A3, S49)])

# ╔═╡ a1cad0c5-1e86-42b1-8467-a4d49f2627a9
Transition_Mod

# ╔═╡ 4abfabb0-b42f-11eb-3d10-2b4c944704d8
#heuristic = map(x->getHeuristic(x),close)

# ╔═╡ 335911f0-b42f-11eb-3f42-539e81e1d27c
#up
function create_result(explored, transm)
	result = []
	for i in 1:(length(explored)-1)
		nextstate = transm[explored[i]]
		for j in nextstate
			if(explored[i+1] == j[2])
				push!(result, j[1])
			end
		end
	end	
	
	
	return result
end

# ╔═╡ 51890660-9917-4060-a92a-52955928e721
function heuristic(cell)
	#add all items left for collection
	sum = 0
	for i in 1:length(cell)
		sum += cell[i]
	end
  return sum
end

# ╔═╡ 209dfee0-b42f-11eb-11d3-3ba804365f6d


# ╔═╡ 7041aaa0-b31c-11eb-1003-6d512b305e01
function astar(initial_state, transition_dict, goal_state)
	
	candidate_selection = Dict() #use to be open
	explored = []
	#storing  value including [f(n),g(n)]
	push!(candidate_selection,initial_state => [heuristic(initial_state.number_of_dirt),0])
	while !(isempty(candidate_selection))
		#get the maximum because collection is the easiest way to reach goal
		current_state = findmax(candidate_selection)
		
		pop!(candidate_selection,current_state[2])
		
		g_score = current_state[1]
		push!(explored,current_state[2])
					
		
		
		neighbours = transition_dict[current_state[2]]
		for neighbour in neighbours
			
			success_current_cost = (g_score[2] + neighbour[1].cost)
			if current_state[2] in goal_state
			   return create_result(explored,transition_dict)
			elseif neighbour[2] in explored 
		       continue
			else
				  push!(candidate_selection, neighbour[2] => [success_current_cost+heuristic(neighbour[2].number_of_dirt),success_current_cost])	
				
			end
		end
	end
end

# ╔═╡ b0eecd00-b167-11eb-1ba9-6920d938f246
astar(S1 ,Transition_Mod,[S30,S50])

# ╔═╡ Cell order:
# ╠═7bc88700-b000-11eb-0302-89ce5dfe6e20
# ╠═ab9cd3d7-5490-4e5d-a5f4-0be85c5bcd2b
# ╠═02e055c7-d1aa-4a33-8d39-d93816e4f959
# ╠═83fb292e-23f7-4c4e-b685-75c457e7a12b
# ╠═1534e8ee-ed5f-4ead-8e0f-090ccebe2269
# ╠═07ddc62a-b941-49e8-91ac-2d39dcb8c436
# ╠═48f37c66-1908-4e2a-b77b-0686556ace56
# ╠═9fad5628-a7d1-417c-a078-a00ee3a19186
# ╠═20d8d5e1-4f8b-4ea8-942e-dcdd953c73f8
# ╠═70740ca2-f361-41da-af94-12f2ce745652
# ╠═47228049-648a-4895-a7f5-5893bbeae3ed
# ╠═48088da2-ad5b-490b-99df-c318ee427913
# ╠═7ee35196-2578-433f-a52a-78ef7bddd2ab
# ╠═3bd1b3bc-b011-4a08-8208-76834ea6d787
# ╠═36e65be3-c104-4790-ab57-57740a6f6863
# ╠═1cd8cbc5-9885-4f0e-aab1-fee4e398e018
# ╠═cc192893-54d4-4a85-95ea-217006df0b55
# ╠═212021b5-3480-49fe-8682-6852e00c0d13
# ╠═9598248c-3d3e-46d4-9a0b-3a84a0823fc0
# ╠═f3e48150-b0da-11eb-3488-2308a2282783
# ╠═86210750-b0db-11eb-0dca-93551e1c97e5
# ╠═a428680e-b0db-11eb-18fa-536ee1b9ff0a
# ╠═be5ca0c0-b0db-11eb-3d55-3959a80d1925
# ╠═ce544e10-b0db-11eb-08a0-838978066845
# ╠═017a0640-b0dc-11eb-1ffd-c158b53475d0
# ╠═07e1bbe0-b0dc-11eb-3326-8b6f0bde15d6
# ╠═19e90f4e-b0dc-11eb-2ec5-ede3edbf46a6
# ╠═2c848c70-b0dc-11eb-0301-99267f65b1ee
# ╠═50f55080-b0dc-11eb-3489-052ca4e4c6b1
# ╠═603dab00-b0dc-11eb-33d8-91eea892bf75
# ╠═72936c90-b0dc-11eb-0b7a-5794ad37087c
# ╠═82d2d140-b0dc-11eb-35f2-4b33e101c9fe
# ╠═956d15e0-b0dc-11eb-26d2-cd5a7fb80401
# ╠═9fa02b60-b0dc-11eb-2bcc-cb5a9678a3ad
# ╠═b25041f0-b0dc-11eb-3320-85e06ba28aa1
# ╠═bc174f30-b0dc-11eb-1141-f5e3b0f12dcb
# ╠═dee33ce0-b0dc-11eb-3c88-e9d8b7f1d36b
# ╠═eac47790-b0dc-11eb-01af-b5579ab866dc
# ╠═66245780-b0e1-11eb-374a-07278d312427
# ╠═f559d150-b0dc-11eb-24be-cbfc215a7ba2
# ╠═ff246100-b0dc-11eb-0a42-53a061c9f18d
# ╠═a18326c0-b0dd-11eb-2da1-6fa5e4b5c329
# ╠═aa5c5190-b0dd-11eb-0c2f-4b7e4fed6582
# ╠═b581dd60-b0dd-11eb-0a5a-190ae5deb928
# ╠═d84a6fb0-b0dd-11eb-0b76-518954ae19ba
# ╠═e6f8d422-b0dd-11eb-0eb8-d5d419d1eeb8
# ╠═f0f437d0-b0dd-11eb-2d8c-2deeb73cc6cd
# ╠═fa72f170-b0dd-11eb-2cfb-a99dd0b6a632
# ╠═0b58f660-b0de-11eb-165d-d9887ad4d8c7
# ╠═19a9d17e-b0de-11eb-0020-6b221513fb21
# ╠═2c71dce2-b0de-11eb-3754-9d82316213c6
# ╠═341ac510-b0de-11eb-3d5c-f9df00ec2842
# ╠═48903250-b0de-11eb-3719-f74a21f20b23
# ╠═55c76010-b0de-11eb-3f8e-970ec370007d
# ╠═6a019912-b0de-11eb-1e5b-3b9525c40138
# ╠═758c9d70-b0de-11eb-363a-c730c48a160b
# ╠═850cf7e0-b0de-11eb-2ffd-f51b6b871855
# ╠═92593440-b0de-11eb-2218-e706ba92d4ac
# ╠═9c28a5f0-b0de-11eb-1c38-c147f95d1f80
# ╠═a29145f0-b0de-11eb-3b5e-b525b7f695d4
# ╠═98b223ef-89a6-4425-bbec-805867c21fe1
# ╠═b7cd3ea9-dec5-4fdb-aa9a-868a8a48ef56
# ╠═2090e0a1-9c47-4ebe-8282-6f61c10b0015
# ╠═46841b1b-f8f2-4d2b-8e1c-2986a8c40086
# ╠═858424cb-ebe3-4363-9c1a-0370689db1a1
# ╠═70ff28a4-34af-4fc2-ae07-dadbfe35bf5c
# ╠═e8a05ca5-857e-458f-a248-92a173321ed5
# ╠═1b2a8067-6e06-4402-88ac-a1215fd85f0a
# ╠═71a079e0-b4e7-4450-ab11-30a9be19da7e
# ╠═e88c01d0-b0de-11eb-06cd-a7b5ed0ffc55
# ╠═18800210-b0df-11eb-3ccf-1123101fb30e
# ╠═35d59f02-b0df-11eb-1258-2303deae91cb
# ╠═53d844d0-b0df-11eb-05b4-61f06de32a9e
# ╠═74c11aa0-b0df-11eb-2daf-53d063f95e3a
# ╠═87ee5070-b0df-11eb-3fef-7ba36a007252
# ╠═aeeb1690-b0df-11eb-39bf-6952d8fabcf7
# ╠═dddd1020-b0df-11eb-0161-ed2a43dfbc49
# ╠═f53ec7e0-b0df-11eb-1c47-a943d073bb20
# ╠═029c906e-b0e0-11eb-038e-0ff1f2b14530
# ╠═15798360-b0e0-11eb-104a-8155eefdd2fa
# ╠═2a52bb7e-b0e0-11eb-229c-43b958c8044e
# ╠═55811d60-b0e0-11eb-3fec-8ba83e9f7d5a
# ╠═714e4592-b0e0-11eb-2430-79cec017dbb4
# ╠═7f830d30-b0e0-11eb-12ee-7b5e70cd4ff8
# ╠═ad938060-b0e0-11eb-2f69-e7c3632a2ee1
# ╠═c7b47f32-b0e0-11eb-0250-87fca7ebf2e8
# ╠═e87f6cc0-b0e0-11eb-38fc-690cdaf3d4df
# ╠═fbb3f590-b0e0-11eb-265e-99cc5f0e141d
# ╠═07ac88ce-b0e1-11eb-2023-6f13406f68ef
# ╠═3dc9af10-b0e1-11eb-33d7-59125ac786aa
# ╠═208d1750-b1b0-11eb-2cfe-bd389943e680
# ╠═9dac5860-b0e1-11eb-20cf-9799286abf46
# ╠═79fa0250-b0e1-11eb-3e1d-396296587878
# ╠═2dc4ecf0-b0e2-11eb-1983-43f43f5fe66d
# ╠═4e318de0-b0e2-11eb-28cc-f13ba0b3d20d
# ╠═6b47fef0-b0e2-11eb-1ca0-e5d79380295a
# ╠═7d899c40-b0e2-11eb-1ff4-7f3557f8f0bd
# ╠═9f99b400-b0e2-11eb-2937-ad49958987fa
# ╠═af1f3e92-b0e2-11eb-1222-45f3e530e39f
# ╠═c6002d90-b0e2-11eb-3f21-db614dc4427d
# ╠═e3d66c30-b0e2-11eb-168f-a9773d38a88f
# ╠═f9239bd0-b0e2-11eb-0dbc-b970be493072
# ╠═099226d0-b0e3-11eb-0bc0-d3f651f8a29c
# ╠═1c5d6680-b0e3-11eb-3dff-1553f970a978
# ╠═593f2f70-b0e3-11eb-25d0-f112b757bcc2
# ╠═6edaa620-b0e3-11eb-2d37-f564ee7c8758
# ╠═7c213d30-b0e3-11eb-1e1d-eb7540938da6
# ╠═90564610-b0e3-11eb-3603-a1c12f247a54
# ╠═a52c49e0-b0e3-11eb-078d-0123d0cbbeea
# ╠═b9543360-b0e3-11eb-0bb3-3b261d9d6294
# ╠═a1cad0c5-1e86-42b1-8467-a4d49f2627a9
# ╟─4abfabb0-b42f-11eb-3d10-2b4c944704d8
# ╠═335911f0-b42f-11eb-3f42-539e81e1d27c
# ╠═51890660-9917-4060-a92a-52955928e721
# ╠═209dfee0-b42f-11eb-11d3-3ba804365f6d
# ╠═7041aaa0-b31c-11eb-1003-6d512b305e01
# ╠═b0eecd00-b167-11eb-1ba9-6920d938f246
