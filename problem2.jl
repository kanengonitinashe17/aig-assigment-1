### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ 276ea6c0-b182-11eb-2bfc-5344b0353b03
using Pkg

# ╔═╡ e6c18cb0-b17b-11eb-298a-39a6c8d8d98b
struct TerminalNode
  utility::Int64
end

# ╔═╡ 249288e0-b182-11eb-1ecc-03d878827715


# ╔═╡ 3c79d310-b17c-11eb-2016-738dec048333
@enum ActionType PMin PMax

# ╔═╡ 3bc9e400-b17c-11eb-2d60-f31342a02f02
struct PlayerNode
  action::ActionType
  nodes::Vector{Union{TerminalNode,PlayerNode}}
end


# ╔═╡ 445c1ac0-b17c-11eb-1ea0-93194edcc1c8
struct Game
  initial::PlayerNode
end

# ╔═╡ 47065330-b17c-11eb-229e-8f8357ec2094
# how to evaluate nodes
function evaluateNode(node::TerminalNode)
  return node.utility
end


# ╔═╡ 537e76b0-b17c-11eb-01f3-715f9f9c6444
function successfunction(state,alpha,beta)
	 if typeof(state) == TerminalNode
		return state.utility
	  end
 
	if state.action == PMax
		bestvalue = -Inf64
		 for stat in state.nodes
			#convert to int 
			 bestvalue = convert(Int64,maximum([successfunction(stat,alpha,beta),bestvalue] ))
	         alpha =  convert(Int64,maximum([alpha,bestvalue] ))
			
			 if beta ≤ alpha
				break
			end
		end
		return bestvalue 
	else
		bestvalue = Inf64
		 for stat in state.nodes
			#convert to int 
			 bestvalue = convert(Int64,minimum([successfunction(stat,alpha,beta),bestvalue] ))
	         beta =  convert(Int64,minimum([beta,bestvalue] ))
			
			 if beta ≤ alpha
				break
			end
		end
		return bestvalue 
	end
end

# ╔═╡ d15b4f70-b17e-11eb-1200-b37e4b63e2e0
begin
node1 = PlayerNode(PMin, [TerminalNode(3), TerminalNode(5), TerminalNode(10)])
node2= PlayerNode(PMin, [TerminalNode(2), TerminalNode(8), TerminalNode(19)])
node3= PlayerNode(PMin, [TerminalNode(2), TerminalNode(7), TerminalNode(3)])
end

# ╔═╡ c6815622-b17f-11eb-1304-3ff4677dc7e9


# ╔═╡ de16966e-b17e-11eb-2d77-8f4ef104325a
node4 = PlayerNode(PMax, [node1, node2, node3])

# ╔═╡ 4f6c9b20-b180-11eb-3167-a7b876d15e0e
alpha = -Inf64

# ╔═╡ 89d59be0-b180-11eb-2d25-dd88fbb27da6
beta = Inf64

# ╔═╡ c67f0c30-b17f-11eb-3611-6fcf44805e21
successfunction(node1,alpha, beta)

# ╔═╡ Cell order:
# ╠═276ea6c0-b182-11eb-2bfc-5344b0353b03
# ╠═e6c18cb0-b17b-11eb-298a-39a6c8d8d98b
# ╠═249288e0-b182-11eb-1ecc-03d878827715
# ╠═3c79d310-b17c-11eb-2016-738dec048333
# ╠═3bc9e400-b17c-11eb-2d60-f31342a02f02
# ╠═445c1ac0-b17c-11eb-1ea0-93194edcc1c8
# ╠═47065330-b17c-11eb-229e-8f8357ec2094
# ╠═537e76b0-b17c-11eb-01f3-715f9f9c6444
# ╠═d15b4f70-b17e-11eb-1200-b37e4b63e2e0
# ╠═c6815622-b17f-11eb-1304-3ff4677dc7e9
# ╠═de16966e-b17e-11eb-2d77-8f4ef104325a
# ╠═4f6c9b20-b180-11eb-3167-a7b876d15e0e
# ╠═89d59be0-b180-11eb-2d25-dd88fbb27da6
# ╠═c67f0c30-b17f-11eb-3611-6fcf44805e21
