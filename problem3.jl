### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ b5e1c7a0-afe0-11eb-305e-d9766837bbba
using Pkg

# ╔═╡ 31ac4b20-b1a4-11eb-340b-b58b2ac2010c
using DataStructures

# ╔═╡ c12eb190-afe0-11eb-170d-4ba49d740d41
Pkg.activate("Project.toml")

# ╔═╡ 25ac1290-b41c-11eb-2843-0551925ce2e7
md"# Structures"

# ╔═╡ 42186ab0-b1dc-11eb-3de6-0f16461691ca

struct ConstantFunctionDict{V}
    value::V
#heyy
    function ConstantFunctionDict{V}(val::V) where V
        return new(val);
    end
end


# ╔═╡ 6533ff00-b1dc-11eb-05f1-17cafe755d90
#for dictionary purposes  fff
mutable struct CSPDict
    dict::Union{Nothing, Dict, ConstantFunctionDict}

    function CSPDict(dictionary::Union{Nothing, Dict, ConstantFunctionDict})
        return new(dictionary);
    end
end

# ╔═╡ 7b77d8c0-b1de-11eb-2325-ff596bf3b3d6
mutable struct CSP 
	variables::AbstractVector
	domains::CSPDict
	neighbors::CSPDict
	constraints::Function
	initial::Tuple
	current_domains::Union{Nothing, Dict}
	nassigns::Int64

    function CSP(vars::AbstractVector, domains::CSPDict, neighbors::CSPDict, constraints::Function;
                initial::Tuple=(), current_domains::Union{Nothing, Dict}=nothing, nassigns::Int64=0)
        return new(vars, domains, neighbors, constraints, initial, current_domains, nassigns)
    end
end

# ╔═╡ c12bcb62-afe0-11eb-05d5-3b63dae23f78
mutable struct CSPVar
	name::String
	#value::Union{Nothing,Any}
	#forbidden_values::Vector{Any}vvvv
	value::Union{Nothing,Any}
	forbidden_values::Vector{Any}
	domain_restriction_count::Int64
end


# ╔═╡ c12ba450-afe0-11eb-1568-b1c8baa29c04
struct nameCSP
	vars::Vector{CSPVar}
	constraints::Vector{Tuple{CSPVar,CSPVar}}
end


# ╔═╡ 965eedb0-b402-11eb-3276-fdaecf34f974
md"## Naive Inference Approach"

# ╔═╡ c12d2af0-afe0-11eb-0bb1-f14d0ea5e898
domain = [1,2,3,4]
 #@enum ColourDomain one two three four

# ╔═╡ 8c18ecce-b1db-11eb-2a40-431b858ede22
function support_pruning(problem, cspDict::CSPDict)
	
  
	value ="x1"
	   for key in problem.variables
		#assign domains to variable 
		if key == "x3"
			  push!( problem.current_domains,Pair(key, ["1"]))
		else
        problem.current_domains = Dict(collect(Pair(key, collect(cspDict.dict.value)) for  key in problem.variables));
		end
		
    end

    nothing;
end

# ╔═╡ 27859740-b1dc-11eb-0204-81e4c4a28679
function parse_neighbors(neighbors::String; variables::AbstractVector=[])
	
    local new_dict = Dict();
    for var in variables
        new_dict[var] = [];
    end
	
    local specs::AbstractVector = collect(map(String, split(spec, [':'])) for spec in split(neighbors, [';']));
    for (A, A_n) in specs
        A = String(strip(A));
        if (!haskey(new_dict, A))
            new_dict[A] = [];
        end
        for B in map(String, split(A_n))
            push!(new_dict[A], B);
            if (!haskey(new_dict, B))
                new_dict[B] = [];
            end
            push!(new_dict[B], A);
        end
    end
    return new_dict;
end

# ╔═╡ af2c8a7e-b1de-11eb-275d-4bc62f529b7e
function different_values_constraint(A::T1, a::T2, B::T1, b::T2) where {T1, T2}
    return (a != b);
end


# ╔═╡ 3fcbde40-b42f-11eb-241a-a5fc4d559b1b


# ╔═╡ 009df4e0-b1de-11eb-0958-e59e3e5a63b7
function XVALUESCSP(colors::AbstractVector, neighbors::String)
	#covert neighbours to dictionary and generate neighbours s
     parsed_neighbors = parse_neighbors(neighbors);
    return CSP(collect(keys(parsed_neighbors)), CSPDict(ConstantFunctionDict{typeof(colors)}(colors)), CSPDict(parsed_neighbors), different_values_constraint);
end

# ╔═╡ 0b5d7ea0-b41d-11eb-13f4-5b1156975f21
md"# Forward Checking  Propagation"

# ╔═╡ f402a040-afe0-11eb-367b-cf9b4475be29
function solve_csp(pb::nameCSP, all_assignments)
		for cur_var in pb.vars
			if cur_var.domain_restriction_count == 4
				return []
			else
				next_val = rand(setdiff(Set([1,2,3,4]),
Set(cur_var.forbidden_values)))

					#assign the value to the variableq
		
			  
			#forward check
				for cur_constraint in pb.constraints
					if !((cur_constraint[1] == cur_var) || (cur_constraint[2] ==
							cur_var))
						continue
					  else
					if cur_constraint[1] == cur_var
								push!(cur_constraint[2].forbidden_values, next_val)
								cur_constraint[2].domain_restriction_count += 1
						# if the count reaches four you should backtrack
						# if the domain is a singleton (count == 3) propagate
                    else
                      push!(cur_constraint[1].forbidden_values, next_val)
                       cur_constraint[1].domain_restriction_count += 1
                     # if the count reaches four you should backtrack
                     # if the domain is a singleton (count == 3) propagate

		          end
              end
          end
       # add the assignment to all_assignments
      push!(all_assignments, cur_var.name => next_val)
      end
   end
return all_assignments
end

# ╔═╡ 12a18610-afe1-11eb-1e82-ed9564cb476b
x1 = CSPVar("X1",nothing, [], 0)


# ╔═╡ 1b81b5c0-afe1-11eb-0e03-5bcb442e9045
x2 = CSPVar("X2", nothing, [], 0)

# ╔═╡ 22bf800e-afe1-11eb-21c2-df33d6264e57
x3 = CSPVar("X3", "1", ["2","3","4"], 0)


# ╔═╡ 29bce5fe-afe1-11eb-383e-45f013e4db39
x4 = CSPVar("X4", nothing, [], 0)

# ╔═╡ 2fcd2cd0-afe1-11eb-3965-9dc868d35014
x5 = CSPVar("X5", nothing, [], 0)

# ╔═╡ 45c43100-afe1-11eb-3dd3-5977d722d286
x6 = CSPVar("X6", nothing, [], 0)

# ╔═╡ 49021df0-afe1-11eb-3ac7-3f3f693f26b7
x7 = CSPVar("X7", nothing, [], 0)


# ╔═╡ 8dee5470-b41d-11eb-3477-4b6ee80bd1b9
md"# Assigning Values For naive "

# ╔═╡ d5a4e5c0-b1db-11eb-0ee0-3dba4e052186
valuesss = XVALUESCSP(["1", "2", "3","4"], "x1: x2 x3 x4 x5 x6; x2: x5; x3: x4; x4: x5 x6; x5: x6; x6: x7 ");

# ╔═╡ 9adb0940-b23c-11eb-1d45-996c1af098f5
support_pruning(valuesss,valuesss.domains)

# ╔═╡ ac324900-b41d-11eb-05f5-d7ee403c9a9b
md"# Assigning Values For Forward Checking Propagation "

# ╔═╡ 52f9d820-afe1-11eb-2c8d-0d7d241b299e
problem = nameCSP([x1, x2, x3, x4, x5, x6, x7], [(x1,x2), (x1,x3), (x1,x4),
(x1,x5), (x1,x6), (x2,x5), (x3,x4), (x4,x5), (x4,x6), (x5,x6), (x6,x7)])



# ╔═╡ 5cace140-b1db-11eb-11a4-3df8e53e1774
function revise(problem, X_i, X_j, removals::Queue{Pair}) 
    local revised::Bool = false;
		
    for x in deepcopy(problem.current_domains[X_i])
		

			
			if ( all((function(y)
                return !problem.constraints(X_i, x, X_j, y);
            end),
            problem.current_domains[X_j]))
		
			#pruning
			  for (i, element) in enumerate(problem.current_domains[X_i])
					#if domain is equal to value remove
					if (element == value)

						deleteat!(problem.current_domains[key], i);
						not_removed = false;

						break;
					end
				end
					#if value does not exist return error 
					if (not_removed)
						error("Could not find ", value, " in ", problem.current_domains[key], " for key '", key, "' to be removed!");
					end

				
        			enqueue!(removals,Pair(key, value));
   
			
			
			
			
			#new
				revised = true;
			
		end
    end
    return revised;
end

# ╔═╡ b5c9d0e0-b1a3-11eb-02a3-a310d5a3e942
function AC3(problem,neighbours::CSPDict) 
	
	
	queue=Queue{Tuple}()
	removals=Queue{Pair}()
#assigning variables to their neighbours
        queue = collect((X_i, X_k) for X_i in problem.variables for X_k in neighbours.dict[X_i]);
   return(queue)
	# end
   support_pruning(problem,problem.domains)
    while (length(queue) != 0)
        local X = pop!(queue);   #Remove the first item from queue
		#
		
        local X_i = X[1];
        local X_j = X[2];
	
        if (revise(problem, X_i, X_j, removals))
			
			#if domain does not contain key return false
            if (!haskey(problem.current_domains, X_i))
			
                return false;
            end
            for X_k in neighbours.dict[X_i]
				
                if (X_k != X_i)
						
                    push!(queue, (X_k, X_i));
                end
            end
        end
    end
    return true;
end

# ╔═╡ 525c8b20-b41d-11eb-3e9d-c12070b41394
md"# Running Naive Inference Approach"

# ╔═╡ f02b4070-b428-11eb-2546-bf21d984606d
valuesss.neighbors.dict

# ╔═╡ ddbcf7f0-b232-11eb-0fab-33b2646af7a9
AC3(valuesss,valuesss.neighbors)

# ╔═╡ 6811cb60-b41d-11eb-143e-3baf571e971e
md"# Running Forward Checking Propagation"

# ╔═╡ 61484240-afe1-11eb-11ef-637b53d25a03
solve_csp(problem, [])

# ╔═╡ Cell order:
# ╠═b5e1c7a0-afe0-11eb-305e-d9766837bbba
# ╠═c12eb190-afe0-11eb-170d-4ba49d740d41
# ╠═31ac4b20-b1a4-11eb-340b-b58b2ac2010c
# ╠═25ac1290-b41c-11eb-2843-0551925ce2e7
# ╠═6533ff00-b1dc-11eb-05f1-17cafe755d90
# ╠═7b77d8c0-b1de-11eb-2325-ff596bf3b3d6
# ╠═42186ab0-b1dc-11eb-3de6-0f16461691ca
# ╠═c12bcb62-afe0-11eb-05d5-3b63dae23f78
# ╠═c12ba450-afe0-11eb-1568-b1c8baa29c04
# ╟─965eedb0-b402-11eb-3276-fdaecf34f974
# ╠═c12d2af0-afe0-11eb-0bb1-f14d0ea5e898
# ╠═8c18ecce-b1db-11eb-2a40-431b858ede22
# ╠═5cace140-b1db-11eb-11a4-3df8e53e1774
# ╠═27859740-b1dc-11eb-0204-81e4c4a28679
# ╠═af2c8a7e-b1de-11eb-275d-4bc62f529b7e
# ╠═3fcbde40-b42f-11eb-241a-a5fc4d559b1b
# ╠═b5c9d0e0-b1a3-11eb-02a3-a310d5a3e942
# ╠═009df4e0-b1de-11eb-0958-e59e3e5a63b7
# ╟─0b5d7ea0-b41d-11eb-13f4-5b1156975f21
# ╠═f402a040-afe0-11eb-367b-cf9b4475be29
# ╠═12a18610-afe1-11eb-1e82-ed9564cb476b
# ╠═1b81b5c0-afe1-11eb-0e03-5bcb442e9045
# ╠═22bf800e-afe1-11eb-21c2-df33d6264e57
# ╠═29bce5fe-afe1-11eb-383e-45f013e4db39
# ╠═2fcd2cd0-afe1-11eb-3965-9dc868d35014
# ╠═45c43100-afe1-11eb-3dd3-5977d722d286
# ╠═49021df0-afe1-11eb-3ac7-3f3f693f26b7
# ╠═8dee5470-b41d-11eb-3477-4b6ee80bd1b9
# ╠═d5a4e5c0-b1db-11eb-0ee0-3dba4e052186
# ╟─9adb0940-b23c-11eb-1d45-996c1af098f5
# ╠═ac324900-b41d-11eb-05f5-d7ee403c9a9b
# ╠═52f9d820-afe1-11eb-2c8d-0d7d241b299e
# ╠═525c8b20-b41d-11eb-3e9d-c12070b41394
# ╠═f02b4070-b428-11eb-2546-bf21d984606d
# ╠═ddbcf7f0-b232-11eb-0fab-33b2646af7a9
# ╠═6811cb60-b41d-11eb-143e-3baf571e971e
# ╠═61484240-afe1-11eb-11ef-637b53d25a03
